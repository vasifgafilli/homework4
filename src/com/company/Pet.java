package com.company;
import java.util.Arrays;

public class Pet {
    public String species;
    public String nickname;
    public int age;
    public int trickLevel;
    public String[] habits;
    // pet's species and nickname
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    // all
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    //empty
    public Pet(){
    }
    //methods of Pet
    public void eat() {
        System.out.println("I am eating");
    }
    public void respond() {
        System.out.printf("Hello,owner.I am %s.I miss you",nickname);
    }
    public void foul() {
        System.out.println("I need to cover it up");
    }
    //toString
    @Override
    public String toString() {
        return String.format("\nPet = %s{nickname = %s,age = %d, trickLevel = %d,habits= %s",species.toUpperCase(), nickname, age, trickLevel, Arrays.toString(habits));
    }
}
