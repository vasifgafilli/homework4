package com.company;
public class Main {

    public static void main(String[] args) {
        String[] habits = {"Sleep","Scratch","Play"};// habit of pet
        String[][] schedule = new String[7][2];// child's schedule
        schedule[0][0]="Monday";
        schedule[0][1]="Speaking day";
        schedule[1][0]="Tuesday";
        schedule[1][1]="Driving day";
        schedule[2][0]="Wednesday";
        schedule[2][1]="Piano day";
        schedule[3][0]="Thursday";
        schedule[3][1]="Football day";
        schedule[4][0]="Friday";
        schedule[4][1]="Voleyball day";
        schedule[5][0]="Saturday";
        schedule[5][1]="Rest day";
        schedule[6][0]="Sunday";
        schedule[6][1]="Friend day";
        Pet cat = new Pet("cat","Patrick",3,110,habits);
        Humans mother = new Humans("Zahid","Ayev",1989);
        Humans father = new Humans("Nahid","Ayev",1997);
        Humans child = new Humans("Rahid","Ayev",2011,110,cat,mother,father,schedule);
        child.greetPet();
        child.describePet();
        cat.eat();
        cat.foul();
        cat.respond();
        System.out.println(cat);
        System.out.println(child);

    }
}
