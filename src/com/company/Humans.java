package com.company;
import java.util.Arrays;
public class Humans {
    //fields
    public String name;
    public String surname;
    public int dateOfBirth;
    public int iqLevel;
    public Pet pet;
    public Humans mother;
    public Humans father;
    public String[][] schedule;
    //all fields
    public Humans(String name, String surname, int dateOfBirth, int iqLevel,Pet pet,Humans mother, Humans father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.iqLevel = iqLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }
    //name,surname and date of birth
    public Humans(String name, String surname, int dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }
    //nam,surname , date og birth ,mother and father
    public Humans(String name, String surname, int dateOfBirth,Humans mother,Humans father) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.mother = mother;
        this.father = father;
    }
    //empty
    public Humans() { }
    //methods
    public void greetPet() {
        System.out.printf("\nHello,%s", pet.nickname);
    }
    public void describePet() {
        String slyness;
        if (pet.trickLevel >= 50) {
            slyness = "It's very sly"; }
        else {
            slyness = "It's almost not sly";}
        System.out.printf("\nI have a %s,it's %d years old,it %s", pet.species, pet.age, slyness); }
    @Override
    public String toString() {
        return String.format("Human{name = %s,surname = %s," +
                        "year = %d,iq = %d,mother = %s %s, father = %s %s schedule = %s " +
                        "Pet = %s{nickname = %s,age = %d," +
                        "trickLevel = %d,habits= %s"
                , name, surname, dateOfBirth, iqLevel,
                mother.name,mother.surname, father.name,father.surname,Arrays.deepToString(schedule) ,pet.species, pet.nickname, pet.age, pet.trickLevel, Arrays.toString(pet.habits)); }
}
